/* eslint-disable import/order */

// my app
const app = require('../../app');

// testing dependencies
const request = require('supertest')(app);

// mocks
const { heartbeatsBodyMock } = require('../mocks/heartbeats.mock');

/**
  * Testing /heartbeats endpoint
 */
describe('POST /api/heartbeats', () => {
    let heartbeatBody;

    // set heartbeatBody to valid input before each request
    beforeEach(() => {
        // valid heartbeatBody input
        heartbeatBody = heartbeatsBodyMock;
    });

    it('respond with OK 200', (done) => {
        request
            .post('/api/heartbeats')
            .send(heartbeatBody)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // no device_id
    it('respond with 422 because of missing device_id', (done) => {
        delete heartbeatBody.device_id;

        request
            .post('/api/heartbeats')
            .send(heartbeatBody)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<No device_id or not parseable to string>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });
});
