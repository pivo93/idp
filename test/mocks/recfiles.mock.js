// a mock for the form-data input the recfile route expects
exports.recfilesBodyMock = {
    device_id: '1',
    user_vehicle_id: 4,
    sha: 'DAJDKSFGFAdasalj',
    longitude: 15,
    latitude: 12,
};

// a mock for a valid vehicle device token (for authentication)
exports.vehicleDeviceTokenMock = 'abcdefg';
