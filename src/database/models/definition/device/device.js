// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.device;

/**
 * @typedef {Object} device
 *
 * @property {string} id the ID of the device
 * @property {string} device_type the device type ('smartphone' or 'obd')
 * @property {string} name the name of the device
 * @property {number} current_obd_firmware_version_id the ID of the current firmware version of the device
 * @property {string} current_obd_config_filename the name of the current config file
 * @property {Date} last_firmware_update the date of the last firmware update
 * @property {Date} last_config_update the date of the last config update
 *
 * @return {device}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('device', {
    id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
    },
    device_type: {
        type: DataTypes.ENUM('smartphone', 'obd'),
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    current_obd_firmware_version_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
            model: 'obd_firmware_version',
            key: 'id',
        },
    },
    current_obd_config_filename: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    last_firmware_update: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    last_config_update: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    schema: tableSchema,
    tableName: 'device',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
