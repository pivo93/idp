// ####### Running this script resets the database and should only be used in a test or dev environment #######

// custom modules
const sequelize = require('.');

// only sync in test or dev environment
if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'test') {
    // !!! Important !!!
    // Syncing sequelize models with db will drop all existing tables and newly creates them so all already inserted data will be lost
    sequelize.sync({ force: true })
        .then(() => console.log('Sequelize models have been synced'))
        .catch(err => console.log(`An error happened while trying to sync Sequelize models: ${err}`));
}
